Name: umanager
Version: RPM_VERSION
Release: RPM_RELEASE
Summary: Contains the UManager manager
License: MIT
Requires: mysql
Provides: umanager

%description
Contains the UManager manager

%build
pyinstaller --onefile %{_sourcedir}/umanager-bin.spec

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_unitdir}
mkdir -p %{buildroot}%{_sysconfdir}/umanager
install -m 755 %(_builddir)dist/umanager %{buildroot}%{_bindir}/umanager
install -m 644 %{_sourcedir}/.cicd/templates/config.ini %{buildroot}%{_sysconfdir}/umanager/config.ini

%files
%{_bindir}/umanager
%config %{_sysconfdir}/umanager

