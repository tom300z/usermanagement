-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Umanager
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Umanager` ;

-- -----------------------------------------------------
-- Schema Umanager
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Umanager` DEFAULT CHARACTER SET utf8 ;
USE `Umanager` ;

-- -----------------------------------------------------
-- Table `Umanager`.`group`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Umanager`.`group` ;

CREATE TABLE IF NOT EXISTS `Umanager`.`group` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `state` VARCHAR(45) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Umanager`.`host`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Umanager`.`host` ;

CREATE TABLE IF NOT EXISTS `Umanager`.`host` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `hostid` VARCHAR(45) NOT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Umanager`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Umanager`.`user` ;

CREATE TABLE IF NOT EXISTS `Umanager`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `public_key` VARCHAR(4000) NULL DEFAULT NULL,
  `private_key` VARCHAR(4000) NULL DEFAULT NULL,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NULL DEFAULT NULL,
  `state` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `Umanager`.`usergroups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Umanager`.`usergroups` ;

CREATE TABLE IF NOT EXISTS `Umanager`.`usergroups` (
  `usergroupsId` INT(11) NOT NULL AUTO_INCREMENT,
  `userLogin` VARCHAR(45) NOT NULL,
  `groupName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`usergroupsId`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

CREATE USER 'root'@"%" IDENTIFIED BY "password";
GRANT ALL PRIVILEGES ON *.* TO 'root'@"%" IDENTIFIED BY "password";