import os
import re
import sys
import string
import random
import logging
import platform
import configparser

# Import a MySQL connector, both MySQLdb and pymysql are supported.
try:
    import pymysql as MySQLdb

    MySQLdb.install_as_MySQLdb()
except ImportError:
    import MySQLdb
    import MySQLdb._exceptions


def fullpath(path):
    """Return the full path of a relative path."""
    if getattr(sys, 'frozen', False):
        application_path = sys._MEIPASS
    else:
        application_path = os.path.dirname(os.path.dirname(__file__))
    if str(path)[0] == "/":
        return path
    else:
        return os.path.join(application_path, path)


def exitprogramm():
    """Exit the program."""
    print("Saving config and exiting Umanager!")
    sys.exit()


def randomstring(length=10):
    """Generate a random string of a fixed length."""
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


# Reads the default config files.
conf = configparser.ConfigParser(comment_prefixes='/', allow_no_value=True)
conf.read(fullpath("default_config.ini"))
conf.read(fullpath(conf["Misc"]["config_file"]))

def writeconfig():
    """Write the configuration file"""
    writeconffile = open(conf["Misc"]["config_file"], "w")
    conf.write(writeconffile)


# Creates and configures a logger object.
log = logging.getLogger()
log.setLevel(logging.INFO)
fileformatter = logging.Formatter("[%(asctime)s] [%(levelname)-7s] - %(message)s", "%Y-%m-%d %H:%M:%S")
streamformatter = logging.Formatter("%(levelname)s: %(message)s")
fh = logging.FileHandler(fullpath(conf["Logging"]["log_file"]))
ch = logging.StreamHandler()
fh.setFormatter(fileformatter)
ch.setFormatter(streamformatter)
fh.setLevel(conf["Logging"]["file_log_level"])
ch.setLevel(conf["Logging"]["console_log_level"])
if conf["Logging"]["file_logging"]:
    log.addHandler(fh)
if conf["Logging"]["console_logging"]:
    log.addHandler(ch)


class ShutdownHandler(logging.Handler):
    """A simple logging handler that exits the program when called."""
    def emit(self, record):
        print(" --- Fatal error, program aborted --- ", file=sys.stderr)
        logging.shutdown()
        exitprogramm()

sh = ShutdownHandler(level=logging.ERROR)
log.addHandler(sh)



