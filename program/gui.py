from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *
from program.meth import *


def setLatestLogMsg(msg):
    global latest_log_msg
    latest_log_msg = str(msg)



class Tk_handler(logging.Handler):
    """A simple logging handler that shows the log message in an tkinter messagebox."""
    def emit(self, record, level=logging.NOTSET):
        print(level)
        setLatestLogMsg(fileformatter.format(record))
        if record.levelno > logging.WARNING and level > logging.WARNING:
            messagebox.showerror("ERROR",self.format(record) + "\n\n_fatal error, program aborted")
            exitprogramm()
        elif record.levelno > logging.INFO and level > logging.INFO:
            messagebox.showwarning("WARNING", self.format(record))
        elif record.levelno <= logging.INFO and level <= logging.INFO:
            messagebox.showinfo("INFO", self.format(record))


log.removeHandler(sh)
th = Tk_handler()
th.setLevel(conf["Logging"]["gui_log_level"])
log.addHandler(th)
setLatestLogMsg("")


def treeview_sort_column(tv, col, reverse):
    l = [(tv.set(k, col), k) for k in tv.get_children('')]
    l.sort(reverse=reverse)

    for index, (val, k) in enumerate(l):
        tv.move(k, '', index)

    tv.heading(col,
               command=lambda: treeview_sort_column(tv, col, not reverse))


class Window(object):
    def __init__(self, top=None, hidden=False, title="UManager"):
        if top:
            self.root = Toplevel(top)
            self.root.grab_set()
        else:
            self.root = Tk()
        if hidden:
            self.root.withdraw()
        self.root.title(title)
        self.mainframe = Frame(self.root, borderwidth=3)
        self.mainframe.pack(fill="both", expand=1)

        self.user_image = PhotoImage(file=fullpath("assets/user96.gif"))
        self.group_image = PhotoImage(file=fullpath("assets/group96.gif"))
        self.host_image = PhotoImage(file=fullpath("assets/host96.gif"))

        self.root.tk.call('wm', 'iconphoto', self.root._w, self.group_image)

    def open(self):
        self.root.mainloop()

    def close(self):
        self.root.destroy()


class WinListEdit(Window):
    def __init__(self, top, hidden=False, title="UManager", lab_txt="", list=[], list_selected=[]):
        super(WinListEdit, self).__init__(top, hidden, title)
        self.root.resizable(False, False)
        self.value = []
        self.top = top
        self.lab = Label(self.mainframe, text=lab_txt, wraplength=200)
        self.lab.pack(padx=2, pady=2)
        self.list = list

        for i in self.list:
            exec("""self.root.cb_group_{n} = StringVar()
self.cb_group_{n} = Checkbutton(self.mainframe, variable=self.root.cb_group_{n}, onvalue="True", offvalue="False", text="{n}")
self.cb_group_{n}.pack(padx=2, pady=2)
if "{n}" in list_selected:
    self.root.cb_group_{n}.set("True")""".format(n=i))

        self.but = Button(self.mainframe, text="Ok", command=self.ok)
        self.but.pack(padx=2, pady=2)


    def ok(self, event=None):
        for i in self.list:
            val = eval("""self.root.cb_group_{n}.get()""".format(n=i))
            if val == "True":
                self.value.append(i)
        self.close()

    def __repr__(self):
        self.top.wait_window(self.root)
        return str(self.value)

    def __list__(self):
        self.top.wait_window(self.root)
        return self.value

class WinYesNoPopup(Window):
    def __init__(self, top, hidden=False, title="UManager", lab_txt=""):
        super(WinYesNoPopup, self).__init__(top, hidden, title)
        self.root.resizable(False, False)
        self.value = False
        self.top = top
        self.lab = Label(self.mainframe, text=lab_txt, wraplength=200)
        self.lab.grid(column=0, row=0, columnspan=2, padx=2, pady=2)

        self.but_yes = Button(self.mainframe, text="Yes", command=self.yes)
        self.but_yes.grid(column=0, row=1,  padx=2, pady=2)

        self.but_no = Button(self.mainframe, text="No", command=self.no)
        self.but_no.grid(column=1, row=1, padx=2, pady=2)

        self.but_yes.focus_set()

    def yes(self, event=None):
        self.value = True
        self.close()

    def no(self, event=None):
        self.value = False
        self.close()

    def __bool__(self):
        self.top.wait_window(self.root)
        return self.value


class WinEntryPopup(Window):
    def __init__(self, top, hidden=False, title="UManager", lab_txt="", but_txt="Ok"):
        super(WinEntryPopup, self).__init__(top, hidden, title)
        self.root.resizable(False, False)
        self.value = ""
        self.top = top
        self.lab = Label(self.mainframe, text=lab_txt, wraplength=200)
        self.lab.pack(padx=2, pady=2)

        self.ent = Entry(self.mainframe, width=32)
        self.ent.pack(padx=2, pady=2)

        self.but = Button(self.mainframe, text=but_txt, command=self.ok)
        self.but.pack(padx=2, pady=2)

        self.ent.bind("<Return>", self.ok)
        self.ent.focus_set()

    def ok(self, event=None):
        self.value = self.ent.get()
        self.close()

    def __repr__(self):
        self.top.wait_window(self.root)
        return self.value

    def __str__(self):
        self.top.wait_window(self.root)
        return self.value


class WinDBSelect(Window):
    def __init__(self, top=None):
        super(WinDBSelect, self).__init__(top)
        self.root.title("Specify a Database to connect to:")
        self.root.resizable(False, False)
        self.mainframe.columnconfigure(1, weight=1)
        self.mainframe.columnconfigure(2, weight=1)

        self.lab_info = Label(self.mainframe, text="Please enter the credentials for the WinUManager MySQL Database")
        self.lab_info.grid(column=0, columnspan=3, row=0, padx=2, pady=2, sticky="nw")

        self.lab_group = Label(self.mainframe, text="Group/Port:")
        self.lab_group.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.ent_group = Entry(self.mainframe)
        self.ent_group.grid(column=1, row=1, padx=2, pady=2, sticky="nwe")
        self.ent_port = Entry(self.mainframe, width=4)
        self.ent_port.grid(column=2, row=1, padx=2, pady=2, sticky="nwe")

        self.lab_user = Label(self.mainframe, text="User:")
        self.lab_user.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.ent_user = Entry(self.mainframe)
        self.ent_user.grid(column=1, columnspan=2, row=2, padx=2, pady=2, sticky="nwe")

        self.lab_pass = Label(self.mainframe, text="Password:")
        self.lab_pass.grid(column=0, row=3, padx=2, pady=2, sticky="nw")
        self.ent_pass = Entry(self.mainframe, show="\u2022")
        self.ent_pass.grid(column=1, columnspan=2, row=3, padx=2, pady=2, sticky="nwe")

        self.lab_dB = Label(self.mainframe, text="Database:")
        self.lab_dB.grid(column=0, row=4, padx=2, pady=2, sticky="nw")
        self.lab_dBName = Label(self.mainframe, text="Umanager")
        self.lab_dBName.grid(column=1, columnspan=2, row=4, padx=2, pady=2, sticky="nwe")

        self.but_save = Button(self.mainframe, text="Save", command=self.save_dBInfo)
        self.but_save.grid(column=0, columnspan=3, row=5, padx=2, pady=2)

        self.root.bind("<Return>", self.save_dBInfo)
        self.get_dBInfo()

    def get_dBInfo(self):
        self.ent_group.delete(0, "end")
        self.ent_group.insert(0, conf["Database"]["host"])
        self.ent_port.delete(0, "end")
        self.ent_port.insert(0, conf["Database"]["port"])
        self.ent_user.delete(0, "end")
        self.ent_user.insert(0, conf["Database"]["user"])
        self.ent_pass.delete(0, "end")

    def save_dBInfo(self, event=None):
        conf["Database"]["host"] = self.ent_group.get()
        conf["Database"]["port"] = self.ent_port.get()
        conf["Database"]["user"] = self.ent_user.get()
        conf["Database"]["password"] = self.ent_pass.get()
        writeconfig()
        log.info("Database credentials saved successfully!")
        self.close()

try:
    from program.dbmanager import *
except Exception as err:
    dbwin = WinDBSelect()
    log.warning(err)
    dbwin.open()
    try:
        from program.dbmanager import *
    except Exception as err:
        hiddenroot = Window(hidden=True)
        log.critical(err)


class WinPreferences(Window):
    def __init__(self, top=None):
        super(WinPreferences, self).__init__(top)

        self.tabs = Notebook(self.mainframe)
        self.tabs.pack()


        self.tab_misc = Frame(self.tabs)
        self.tab_misc.columnconfigure(0, weight=1)
        self.tab_misc.columnconfigure(1, weight=1)

        self.lab_misc_info = Label(self.tab_misc, text="Configure miscellaneous things here.")
        self.lab_misc_info.grid(column=0, columnspan=2, row=0, padx=2, pady=2, sticky="nw")

        self.lab_misc_def_int = Label(self.tab_misc, text="Default WinUManager interface: ")
        self.lab_misc_def_int.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.cbx_misc_def_int_values = ["gui", "cli"]
        self.root.cbx_misc_def_int_value = StringVar()
        self.cbx_misc_def_int = Combobox(self.tab_misc, textvariable=self.root.cbx_misc_def_int_value, state='readonly', values=self.cbx_misc_def_int_values)
        self.cbx_misc_def_int.grid(column=1, row=1, padx=2, pady=2, sticky="nwe")

        self.lab_misc_conf_file = Label(self.tab_misc, text="Configuration file: ")
        self.lab_misc_conf_file.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.ent_misc_conf_file = Entry(self.tab_misc)
        self.ent_misc_conf_file.grid(column=1, row=2, padx=2, pady=2, sticky="nwe")

        self.tabs.add(self.tab_misc, text="Miscellaneous", sticky="nesw")


        self.tab_db = Frame(self.tabs)
        self.tab_db.columnconfigure(0, weight=1)
        self.tab_db.columnconfigure(1, weight=1)

        self.lab_db_info = Label(self.tab_db, text="Enter the credentials for the UManager MySQL Database here.")
        self.lab_db_info.grid(column=0, columnspan=2, row=0, padx=2, pady=2, sticky="nw")

        self.lab_db_host = Label(self.tab_db, text="Host: ")
        self.lab_db_host.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.ent_db_host = Entry(self.tab_db)
        self.ent_db_host.grid(column=1, row=1, padx=2, pady=2, sticky="nwe")

        self.lab_db_port = Label(self.tab_db, text="Port: ")
        self.lab_db_port.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.ent_db_port = Entry(self.tab_db)
        self.ent_db_port.grid(column=1, row=2, padx=2, pady=2, sticky="nwe")

        self.lab_db_user = Label(self.tab_db, text="User: ")
        self.lab_db_user.grid(column=0, row=3, padx=2, pady=2, sticky="nw")
        self.ent_db_user = Entry(self.tab_db)
        self.ent_db_user.grid(column=1, row=3, padx=2, pady=2, sticky="nwe")

        self.lab_db_pass = Label(self.tab_db, text="Password: ")
        self.lab_db_pass.grid(column=0, row=4, padx=2, pady=2, sticky="nw")
        self.ent_db_pass = Entry(self.tab_db, show="\u2022")
        self.ent_db_pass.grid(column=1, row=4, padx=2, pady=2, sticky="nwe")

        self.lab_db_db = Label(self.tab_db, text="Database: ")
        self.lab_db_db.grid(column=0, row=5, padx=2, pady=2, sticky="nw")
        self.lab_db_db_name = Label(self.tab_db, text="Umanager")
        self.lab_db_db_name.grid(column=1, row=5, padx=2, pady=2, sticky="nwe")

        self.tabs.add(self.tab_db, text="Database", sticky="nesw")
        
        
        self.tab_logging = Frame(self.tabs)
        self.tab_logging.columnconfigure(0, weight=1)
        self.tab_logging.columnconfigure(1, weight=1)

        self.lab_logging_info = Label(self.tab_logging, text="Configure logging here.")
        self.lab_logging_info.grid(column=0, columnspan=2, row=0, padx=2, pady=2, sticky="nw")

        self.lab_logging_log_file = Label(self.tab_logging, text="Path to log_file:")
        self.lab_logging_log_file.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.ent_logging_log_file = Entry(self.tab_logging)
        self.ent_logging_log_file.grid(column=1, row=1, padx=2, pady=2, sticky="nwe")

        self.cbx_logging_cli_log_lvl = Label(self.tab_logging, text="Console log level:")
        self.cbx_logging_cli_log_lvl.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.cbx_logging_cli_log_lvl_values = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
        self.root.cbx_logging_cli_log_lvl_value = StringVar()
        self.cbx_logging_cli_log_lvl = Combobox(self.tab_logging, textvariable=self.root.cbx_logging_cli_log_lvl_value, state='readonly', values=self.cbx_logging_cli_log_lvl_values)
        self.cbx_logging_cli_log_lvl.grid(column=1, row=2, padx=2, pady=2, sticky="nwe")
        
        self.cbx_logging_file_log_lvl = Label(self.tab_logging, text="File log level:")
        self.cbx_logging_file_log_lvl.grid(column=0, row=3, padx=2, pady=2, sticky="nw")
        self.cbx_logging_file_log_lvl_values = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
        self.root.cbx_logging_file_log_lvl_value = StringVar()
        self.cbx_logging_file_log_lvl = Combobox(self.tab_logging, textvariable=self.root.cbx_logging_file_log_lvl_value, state='readonly', values=self.cbx_logging_file_log_lvl_values)
        self.cbx_logging_file_log_lvl.grid(column=1, row=3, padx=2, pady=2, sticky="nwe")
        
        self.cbx_logging_gui_log_lvl = Label(self.tab_logging, text="GUI log level:")
        self.cbx_logging_gui_log_lvl.grid(column=0, row=4, padx=2, pady=2, sticky="nw")
        self.cbx_logging_gui_log_lvl_values = ["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"]
        self.root.cbx_logging_gui_log_lvl_value = StringVar()
        self.cbx_logging_gui_log_lvl = Combobox(self.tab_logging, textvariable=self.root.cbx_logging_gui_log_lvl_value, state='readonly', values=self.cbx_logging_gui_log_lvl_values)
        self.cbx_logging_gui_log_lvl.grid(column=1, row=4, padx=2, pady=2, sticky="nwe")

        self.cb_logging_console = Label(self.tab_logging, text="Enable console logging:")
        self.cb_logging_console.grid(column=0, row=5, padx=2, pady=2, sticky="nw")
        self.root.cb_logging_console_value = StringVar()
        self.cb_logging_console = Checkbutton(self.tab_logging, variable=self.root.cb_logging_console_value, onvalue="True", offvalue="False")
        self.cb_logging_console.grid(column=1, row=5, padx=2, pady=2, sticky="nwe")
        
        self.cb_logging_file = Label(self.tab_logging, text="Enable file logging:")
        self.cb_logging_file.grid(column=0, row=6, padx=2, pady=2, sticky="nw")
        self.root.cb_logging_file_value = StringVar()
        self.cb_logging_file = Checkbutton(self.tab_logging, variable=self.root.cb_logging_file_value, onvalue="True", offvalue="False")
        self.cb_logging_file.grid(column=1, row=6, padx=2, pady=2, sticky="nwe")

        self.tabs.add(self.tab_logging, text="Logging", sticky="nesw")


        self.tab_user = Frame(self.tabs)
        self.tab_user.columnconfigure(0, weight=1)
        self.tab_user.columnconfigure(1, weight=1)

        self.lab_user_info = Label(self.tab_user, text="Configure settings for new users here.")
        self.lab_user_info.grid(column=0, columnspan=2, row=0, padx=2, pady=2, sticky="nw")

        self.cbx_user_def_pass = Label(self.tab_user, text="Default password for new users:")
        self.cbx_user_def_pass.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.cbx_user_def_pass_values = ["RANDOM"]
        self.root.cbx_user_def_pass_value = StringVar()
        self.cbx_user_def_pass = Combobox(self.tab_user, textvariable=self.root.cbx_user_def_pass_value, values=self.cbx_user_def_pass_values)
        self.cbx_user_def_pass.grid(column=1, row=2, padx=2, pady=2, sticky="nwe")

        self.tabs.add(self.tab_user, text="User", sticky="nesw")


        self.but_save = Button(self.mainframe, text="Save", command=self.save_info)
        self.but_save.pack()
        self.root.bind("<Return>", self.save_info)
        self.get_info()

    def get_info(self):
        self.cbx_misc_def_int.current(self.cbx_misc_def_int_values.index(conf["Misc"]["default_interface"]))
        self.ent_misc_conf_file.delete(0, "end")
        self.ent_misc_conf_file.insert(0, conf["Misc"]["config_file"])

        self.ent_db_host.delete(0, "end")
        self.ent_db_host.insert(0, conf["Database"]["host"])
        self.ent_db_port.delete(0, "end")
        self.ent_db_port.insert(0, conf["Database"]["port"])
        self.ent_db_user.delete(0, "end")
        self.ent_db_user.insert(0, conf["Database"]["User"])
        self.ent_db_pass.delete(0, "end")

        self.ent_logging_log_file.delete(0, "end")
        self.ent_logging_log_file.insert(0, conf["Logging"]["log_file"])
        self.cbx_logging_cli_log_lvl.current(self.cbx_logging_cli_log_lvl_values.index(conf["Logging"]["console_log_level"].upper()))
        self.cbx_logging_file_log_lvl.current(self.cbx_logging_file_log_lvl_values.index(conf["Logging"]["file_log_level"].upper()))
        self.cbx_logging_gui_log_lvl.current(self.cbx_logging_gui_log_lvl_values.index(conf["Logging"]["gui_log_level"].upper()))
        self.root.cb_logging_console_value.set(str(conf["Logging"]["console_logging"]))
        self.root.cb_logging_file_value.set(str(conf["Logging"]["file_logging"]))
        self.root.cbx_user_def_pass_value.set(str(conf["User"]["default_password"]))

    def save_info(self, event=None):
        conf["Misc"]["default_interface"] = self.root.cbx_misc_def_int_value.get()
        conf["Misc"]["config_file"] = self.ent_misc_conf_file.get()

        conf["Database"]["host"] = self.ent_db_host.get()
        conf["Database"]["port"] = self.ent_db_port.get()
        conf["Database"]["user"] = self.ent_db_user.get()
        conf["Database"]["password"] = self.ent_db_pass.get()

        conf["Logging"]["log_file"] = self.ent_logging_log_file.get()
        conf["Logging"]["console_log_level"] = self.cbx_logging_cli_log_lvl.get()
        conf["Logging"]["file_log_level"] = self.cbx_logging_file_log_lvl.get()
        conf["Logging"]["gui_log_level"] = self.cbx_logging_gui_log_lvl.get()
        conf["Logging"]["console_logging"] = self.root.cb_logging_console_value.get()
        conf["Logging"]["file_logging"] = self.root.cb_logging_file_value.get()
        conf["User"]["default_password"] = self.root.cbx_user_def_pass_value.get()

        writeconfig()
        self.close()
        log.info("Preferences saved successfully!")


class WinHosts(Window):
    def __init__(self, top=None):
        super(WinHosts, self).__init__(top)
        self.panes = PanedWindow(self.mainframe, orient=HORIZONTAL)
        self.panes.pack(fill=BOTH, expand=1)

        self.fra_list = Labelframe(self.panes, text="Select a host")

        self.fra_list.columnconfigure(0, weight=1)
        self.fra_list.columnconfigure(1, weight=1)
        self.fra_list.rowconfigure(0, weight=1)

        self.tre_list_hostlist = Treeview(self.fra_list)
        self.tre_list_hostlist.heading("#0", text="Host login")
        self.tre_list_hostlist.grid(column=0, row=0, columnspan=2, padx=2, pady=2, sticky="nesw")
        self.tre_list_hostlist.bind("<<TreeviewSelect>>", self.select)

        self.but_list_add = Button(self.fra_list, text="Add host", command=self.add_host)
        self.but_list_add.grid(column=0, row=1, padx=2, pady=2, sticky="ew")

        self.but_list_remove = Button(self.fra_list, text="Remove host", command=self.remove_host)
        self.but_list_remove.grid(column=1, row=1, padx=2, pady=2, sticky="ew")

        self.panes.add(self.fra_list)

        separator = Separator(self.mainframe, orient=HORIZONTAL)
        separator.pack(fill=X, padx=2)
        self.latest_log_msg_txt = StringVar()
        self.lab_last_log = Label(self.mainframe, textvariable=self.latest_log_msg_txt)
        self.lab_last_log.pack(fill=X)

        self.select()

    def select(self, event=None):
        self.get_list_data()
        self.selected_host_list = list(self.tre_list_hostlist.selection())
        self.get_last_log_msg()

    def get_last_log_msg(self, event=None):
        fh=open(fullpath(conf["Logging"]["log_file"]), "rb")
        fh.seek(-1024, 2)
        last = fh.readlines()[-1].decode().splitlines()[0]
        self.latest_log_msg_txt.set(last)

    def get_list_data(self, event=None):
        treelist = list(self.tre_list_hostlist.get_children())
        hostlist = sorted(List().hosts())
        if treelist != hostlist:
            for i in treelist:
                if i not in hostlist:
                    self.tre_list_hostlist.delete(i)
            for i in hostlist:
                if i not in treelist:
                    self.tre_list_hostlist.insert("", "end", iid=i, text=i)

    def add_host(self, event=None):
        new_hosts = str(WinEntryPopup(top=self.root, title="Add host(s)", lab_txt="Please enter login(s) separated by \",\"", but_txt="Add host(s)"))
        new_host_list = new_hosts.split(",")
        for hostid in new_host_list:
            host = Host()
            host.hostid = str(hostid)
            host.add()
        self.get_list_data()
        self.tre_list_hostlist.focus(new_host_list[0])
        self.tre_list_hostlist.selection_set(tuple(new_host_list))
        self.get_last_log_msg()
        self.root.focus_set()
        self.root.grab_set()

    def remove_host(self, event=None):
        hostlist = self.selected_host_list
        if WinYesNoPopup(top=self.root, title="Delete host(s)?", lab_txt="Do you really want to delete the following hosts:\n{}".format(",".join(hostlist))):
            for hostid in hostlist:
                host = Host(hostid=hostid)
                host.remove()
            self.get_list_data()
            self.get_last_log_msg()
            self.root.focus_set()
            self.root.grab_set()


class WinGroups(Window):
    def __init__(self, top=None):
        super(WinGroups, self).__init__(top)
        self.panes = PanedWindow(self.mainframe, orient=HORIZONTAL)
        self.panes.pack(fill=BOTH, expand=1)


        self.fra_list = Labelframe(self.panes, text="Select a group")

        self.fra_list.columnconfigure(0, weight=1)
        self.fra_list.columnconfigure(1, weight=1)
        self.fra_list.rowconfigure(0, weight=1)

        self.tre_list_grouplist = Treeview(self.fra_list)
        self.tre_list_grouplist.heading("#0", text="Group login")
        self.tre_list_grouplist.grid(column=0, row=0, columnspan=2, padx=2, pady=2, sticky="nesw")
        self.tre_list_grouplist.bind("<<TreeviewSelect>>", self.select)

        self.but_list_add = Button(self.fra_list, text="Add group", command=self.add_group)
        self.but_list_add.grid(column=0, row=1, padx=2, pady=2, sticky="ew")

        self.but_list_remove = Button(self.fra_list, text="Remove group", command=self.remove_group)
        self.but_list_remove.grid(column=1, row=1, padx=2, pady=2, sticky="ew")

        self.panes.add(self.fra_list)


        self.fra_details = Labelframe(self.panes, text="Modify group \"\"")

        self.fra_details.columnconfigure(0, weight=2)
        self.fra_details.columnconfigure(1, weight=1)
        self.fra_details.columnconfigure(2, weight=1)

        self.lab_details_name = Label(self.fra_details, text="Login(s): ")
        self.lab_details_name.grid(column=0, row=0, padx=2, pady=2, sticky="nw")
        self.lab_details_name_val = Label(self.fra_details, text="")
        self.lab_details_name_val.grid(column=1, row=0, columnspan=2, padx=2, pady=2, sticky="new")

        self.lab_details_members = Label(self.fra_details, text="Members: ")
        self.lab_details_members.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.lab_details_members_value = Label(self.fra_details, text="", wraplength=200)
        self.lab_details_members_value.grid(column=1, row=1, columnspan=2, padx=2, pady=2, sticky="nw")
        self.but_details_members = Button(self.fra_details, text="Manage members", command=self.update_members)
        self.but_details_members.grid(column=1, row=2, columnspan=2, padx=2, pady=2, sticky="nw")

        self.panes.add(self.fra_details)

        separator = Separator(self.mainframe, orient=HORIZONTAL)
        separator.pack(fill=X, padx=2)
        self.latest_log_msg_txt = StringVar()
        self.lab_last_log = Label(self.mainframe, textvariable=self.latest_log_msg_txt)
        self.lab_last_log.pack(fill=X)

        self.selected_group_list = []
        self.select()

    def select(self, event=None):
        self.selected_group_list = list(self.tre_list_grouplist.selection())
        if len(self.selected_group_list) > 0:
            self.cur_group = Group(name=self.selected_group_list[0])
        else:
            self.cur_group = Group()
        self.get_list_data()
        self.get_group_data()
        self.get_last_log_msg()

    def get_last_log_msg(self, event=None):
        fh=open(fullpath(conf["Logging"]["log_file"]), "rb")
        fh.seek(-1024, 2)
        last = fh.readlines()[-1].decode().splitlines()[0]
        self.latest_log_msg_txt.set(last)

    def get_list_data(self, event=None):
        treelist = list(self.tre_list_grouplist.get_children())
        grouplist = sorted(List().groups())
        if treelist != grouplist:
            for i in treelist:
                if i not in grouplist:
                    self.tre_list_grouplist.delete(i)
            for i in grouplist:
                if i not in treelist:
                    self.tre_list_grouplist.insert("", "end", iid=i, text=i)

    def get_group_data(self, event=None):
        selection = self.selected_group_list
        if len(selection) <= 1:
            self.lab_details_name_val["text"] = self.cur_group.name
            self.fra_details["text"] = "Modify group \"{}\"".format(self.cur_group.name)
            self.lab_details_members_value["text"] = ",".join(self.cur_group.memberlist())
        else:
            self.lab_details_name_val["text"] = ",".join(selection)

    def add_group(self, event=None):
        new_groups = str(WinEntryPopup(top=self.root, title="Add group(s)", lab_txt="Please enter group name(s) separated by \",\"", but_txt="Add group(s)"))
        new_group_list = new_groups.split(",")
        for groupname in new_group_list:
            group = Group()
            group.name = str(groupname)
            group.add()
        self.get_list_data()
        self.tre_list_grouplist.focus(new_group_list[0])
        self.get_group_data()
        self.tre_list_grouplist.selection_set(tuple(new_group_list))
        self.get_last_log_msg()
        self.root.focus_set()
        self.root.grab_set()

    def remove_group(self, event=None):
        grouplist = self.selected_group_list
        if WinYesNoPopup(top=self.root, title="Delete group(s)?", lab_txt="Do you really want to delete the following groups:\n{}".format(",".join(grouplist))):
            for groupname in grouplist:
                group = Group(name=groupname)
                if group.name not in List().users():
                    group.remove()
                else:
                    log.warning("Can't remove the primary group of user \"{}\"".format(group.name))
            self.get_list_data()
            self.get_group_data()
            self.get_last_log_msg()
            self.root.focus_set()
            self.root.grab_set()

    def update_members(self, event=None):
        checked_members = list(WinListEdit(top=self.root, title="Select members",
                                           lab_txt="Select members for the group(s):\n{}".format(",".join(self.selected_group_list)),
                                           list=(List().users()),
                                           list_selected=self.cur_group.memberlist()).__list__())
        for g in self.selected_group_list:
            group = Group(name=g)
            for m in checked_members:
                if m not in group.memberlist():
                    if g not in List().users():
                        group.adduser(User(login=m))
                    else:
                        log.warning("Cannot add user \"{}\" to the primary group \"{}\".".format(m, g))
            for m in group.memberlist():
                if m not in checked_members:
                    if g not in List().users():
                        group.removeuser(User(login=m))
                    else:
                        log.warning("Cannot remove user \"{}\" from the primary group \"{}\".".format(m, g))
        self.get_group_data()
        self.get_last_log_msg()
        self.root.focus_set()
        self.root.grab_set()


class WinUsers(Window):
    def __init__(self, top=None):
        super(WinUsers, self).__init__(top)
        self.panes = PanedWindow(self.mainframe, orient=HORIZONTAL)
        self.panes.pack(fill=BOTH, expand=1)


        self.fra_list = Labelframe(self.panes, text="Select a user")

        self.fra_list.columnconfigure(0, weight=1)
        self.fra_list.columnconfigure(1, weight=1)
        self.fra_list.rowconfigure(0, weight=1)

        self.tre_list_userlist = Treeview(self.fra_list)
        self.tre_list_userlist.heading("#0", text="User login")
        self.tre_list_userlist.grid(column=0, row=0, columnspan=2, padx=2, pady=2, sticky="nesw")
        self.tre_list_userlist.bind("<<TreeviewSelect>>", self.select)

        self.but_list_add = Button(self.fra_list, text="Add user", command=self.add_user)
        self.but_list_add.grid(column=0, row=1, padx=2, pady=2, sticky="ew")

        self.but_list_remove = Button(self.fra_list, text="Remove user", command=self.remove_user)
        self.but_list_remove.grid(column=1, row=1, padx=2, pady=2, sticky="ew")

        self.but_list_update = Button(self.fra_list, text="Update selected user(s)", command=self.update_selected)
        self.but_list_update.grid(column=0, row=2, columnspan=2, padx=2, pady=2, sticky="ew")

        self.panes.add(self.fra_list)


        self.fra_details = Labelframe(self.panes, text="Modify user \"\"")

        self.fra_details.columnconfigure(0, weight=2)
        self.fra_details.columnconfigure(1, weight=1)
        self.fra_details.columnconfigure(2, weight=1)

        self.lab_details_login = Label(self.fra_details, text="Login(s): ")
        self.lab_details_login.grid(column=0, row=0, padx=2, pady=2, sticky="nw")
        self.lab_details_login_val = Label(self.fra_details, text="")
        self.lab_details_login_val.grid(column=1, row=0, columnspan=2, padx=2, pady=2, sticky="new")

        self.lab_details_pub_key = Label(self.fra_details, text="Public key (ssh-rsa ...): ")
        self.lab_details_pub_key.grid(column=0, row=1, padx=2, pady=2, sticky="nw")
        self.txt_details_pub_key = Text(self.fra_details, width=32, height=4)
        self.txt_details_pub_key.grid(column=1, row=1, columnspan=2, padx=2, pady=2, sticky="new")

        self.lab_details_priv_key = Label(self.fra_details, text="Private key (-----BEGIN RSA PRIV...): ")
        self.lab_details_priv_key.grid(column=0, row=2, padx=2, pady=2, sticky="nw")
        self.txt_details_priv_key = Text(self.fra_details, width=32, height=4)
        self.txt_details_priv_key.grid(column=1, row=2, columnspan=2, padx=2, pady=2, sticky="new")

        self.lab_details_password = Label(self.fra_details, text="Password: ")
        self.lab_details_password.grid(column=0, row=3, padx=2, pady=2, sticky="nw")
        self.ent_details_password = Entry(self.fra_details)
        self.ent_details_password.grid(column=1, row=3, columnspan=2, padx=2, pady=2, sticky="new")

        self.lab_details_state = Label(self.fra_details, text="State: ")
        self.lab_details_state.grid(column=0, row=4, padx=2, pady=2, sticky="nw")
        self.root.rb_details_state_value = StringVar()
        self.rb_details_state_ena = Radiobutton(self.fra_details, text="enabled", variable=self.root.rb_details_state_value, value="ENABLED")
        self.rb_details_state_ena.grid(column=1, row=4, padx=2, pady=2, sticky="new")
        self.rb_details_state_dis = Radiobutton(self.fra_details, text="disabled", variable=self.root.rb_details_state_value, value="DISABLED")
        self.rb_details_state_dis.grid(column=2, row=4, padx=2, pady=2, sticky="new")

        self.lab_details_groups = Label(self.fra_details, text="Groups: ")
        self.lab_details_groups.grid(column=0, row=5, padx=2, pady=2, sticky="nw")
        self.lab_details_groups_value = Label(self.fra_details, text="", wraplength=200)
        self.lab_details_groups_value.grid(column=1, row=5, columnspan=2, padx=2, pady=2, sticky="nw")
        self.but_details_groups = Button(self.fra_details, text="Manage groups", command=self.update_groups)
        self.but_details_groups.grid(column=1, row=6, columnspan=2, padx=2, pady=2, sticky="nw")

        self.panes.add(self.fra_details)

        separator = Separator(self.mainframe, orient=HORIZONTAL)
        separator.pack(fill=X, padx=2)
        self.latest_log_msg_txt = StringVar()
        self.lab_last_log = Label(self.mainframe, textvariable=self.latest_log_msg_txt)
        self.lab_last_log.pack(fill=X)

        self.selected_user_list = []
        self.select()

    def select(self, event=None):
        self.get_list_data()
        self.selected_user_list = list(self.tre_list_userlist.selection())
        if len(self.selected_user_list) > 0:
            self.cur_user = User(login=self.selected_user_list[0])
        else:
            self.cur_user = User()
        self.get_user_data()
        self.get_last_log_msg()

    def get_last_log_msg(self, event=None):
        fh=open(fullpath(conf["Logging"]["log_file"]), "rb")
        fh.seek(-1024, 2)
        last = fh.readlines()[-1].decode().splitlines()[0]
        self.latest_log_msg_txt.set(last)

    def get_list_data(self, event=None):
        treelist = list(self.tre_list_userlist.get_children())
        userlist = sorted(List().users())
        if treelist != userlist:
            for i in treelist:
                if i not in userlist:
                    self.tre_list_userlist.delete(i)
            for i in userlist:
                if i not in treelist:
                    self.tre_list_userlist.insert("", "end", iid=i, text=i)

    def get_user_data(self, event=None):
        if len(self.selected_user_list) <= 1:
            self.lab_details_login_val["text"] = self.cur_user.login
            self.fra_details["text"] = "Modify user \"{}\"".format(self.cur_user.login)
            self.txt_details_pub_key.delete(1.0, "end")
            self.txt_details_pub_key.insert(1.0, self.cur_user.public_key)
            self.txt_details_priv_key.delete(1.0, "end")
            self.txt_details_priv_key.insert(1.0, self.cur_user.private_key)
            self.ent_details_password.delete(0, "end")
            self.ent_details_password.insert(0, self.cur_user._password)
            self.lab_details_groups_value["text"] = ",".join(self.cur_user.grouplist())
        else:
            self.lab_details_login_val["text"] = ",".join(self.selected_user_list)
        if self.cur_user.state != "DELETED":
            self.root.rb_details_state_value.set(self.cur_user.state)

    def update_selected(self, event=None):
        userlist = self.selected_user_list
        if len(self.selected_user_list) > 0:
            for login in userlist:
                user = User(login=str(login))
                user.public_key = str(self.txt_details_pub_key.get("1.0",END))
                user.private_key = str(self.txt_details_priv_key.get("1.0",END))
                user._password = str(self.ent_details_password.get())
                user.state = str(self.root.rb_details_state_value.get())
                user.update()
            self.tre_list_userlist.focus(self.selected_user_list[0])
            self.root.focus_set()
            self.root.grab_set()
        else:
            log.info("Please select one or more users to update")
        self.get_last_log_msg()

    def add_user(self, event=None):
        new_users = str(WinEntryPopup(top=self.root, title="Add user(s)", lab_txt="Please enter login(s) separated by \",\"", but_txt="Add user(s)"))
        new_user_list = new_users.split(",")
        for username in new_user_list:
            user = User()
            user.login = username
            if conf.has_option("User", "default_password"):
                if conf["User"]["default_password"] == "RANDOM":
                    password = randomstring(length=10)
                else:
                    password = str(conf["User"]["default_password"])
            else:
                password = ""
            user._password = password
            user.add()
            group = Group(name=user.login)
            group.add()
            group.adduser(user)
        self.get_list_data()
        self.tre_list_userlist.focus(new_user_list[0])
        self.get_user_data()
        self.tre_list_userlist.selection_set(tuple(new_user_list))
        self.get_last_log_msg()
        self.root.focus_set()
        self.root.grab_set()

    def remove_user(self, event=None):
        userlist = self.selected_user_list
        if WinYesNoPopup(top=self.root, title="Delete user(s)?", lab_txt="Do you really want to delete the following users:\n{}".format(",".join(userlist))):
            for username in userlist:
                user = User(login=username)
                group = Group(name=user.login)
                group.removeuser(user)
                group.remove()
                user.remove()
            self.get_list_data()
            self.get_user_data()
            self.get_last_log_msg()
            self.root.focus_set()
            self.root.grab_set()

    def update_groups(self, event=None):
        checked_groups = list(WinListEdit(top=self.root, title="Select groups",
                                           lab_txt="Select groups for the user(s):\n{}".format(
                                               ",".join(self.selected_user_list)),
                                           list=(List().groups()),
                                           list_selected=self.cur_user.grouplist()).__list__())
        for u in self.selected_user_list:
            for g in checked_groups:
                group = Group(name=g)
                if u not in group.memberlist():
                    if g not in List().users():
                        group.adduser(User(login=u))
                    else:
                        log.warning("Cannot add user \"{}\" to the primary group \"{}\".".format(u, g))
            for g in List().groups():
                group = Group(name=g)
                if g not in checked_groups and u in group.memberlist():
                    if g not in List().users():
                        group.removeuser(User(login=u))
                    else:
                        log.warning("Cannot remove user \"{}\" from the primary group \"{}\".".format(u, g))
        self.get_user_data()
        self.get_last_log_msg()
        self.root.focus_set()
        self.root.grab_set()


class WinUManager(Window):
    def __init__(self):
        super(WinUManager, self).__init__()
        self.root.resizable(False, False)
        self.menu_bar = Menu(self.root)
        self.menu_file = Menu(self.menu_bar, tearoff=0, activebackground="#91c9f7", activeforeground="black")
        self.menu_file.add_command(label="Exit", command=self.close)
        self.menu_bar.add_cascade(label="File", menu=self.menu_file)

        self.menu_edit = Menu(self.menu_bar, tearoff=0, activebackground="#91c9f7", activeforeground="black")
        self.menu_edit.add_command(label="Preferences", command=lambda: WinPreferences(top=self.root))
        self.menu_bar.add_cascade(label="Edit", menu=self.menu_edit)

        self.menu_help = Menu(self.menu_bar, tearoff=0, activebackground="#91c9f7", activeforeground="black")
        self.menu_help.add_command(label="About UManager", command=lambda: log.info("About popup currently unimplemented"))
        self.menu_help.add_command(label="Manual", command=lambda: log.info("Manual currently unimplemented"))
        self.menu_bar.add_cascade(label="Help", menu=self.menu_help)
        self.root.config(menu=self.menu_bar)

        self.lab_info = Label(self.mainframe, text="Select a task:")
        self.lab_info.grid(column=0, columnspan=3, row=0, padx=2, pady=2, sticky="nw")

        self.but_users = Button(self.mainframe, image=self.user_image, command=lambda: WinUsers(top=self.root))
        self.but_users.grid(column=0, row=1, padx=2, pady=2)
        self.lab_users = Label(self.mainframe, text="Add/delete/edit user(s)", wraplength=96, justify=CENTER)
        self.lab_users.grid(column=0, row=2, padx=2, pady=2)

        self.but_groups = Button(self.mainframe, image=self.group_image, command=lambda: WinGroups(top=self.root))
        self.but_groups.grid(column=1, row=1, padx=2, pady=2)
        self.lab_groups = Label(self.mainframe, text="Add/delete/edit group(s)", wraplength=96, justify=CENTER)
        self.lab_groups.grid(column=1, row=2, padx=2, pady=2)

        self.but_hosts = Button(self.mainframe, image=self.host_image, command=lambda: WinHosts(top=self.root))
        self.but_hosts.grid(column=2, row=1, padx=2, pady=2)
        self.lab_hosts = Label(self.mainframe, text="Add/delete host(s)", wraplength=96, justify=CENTER)
        self.lab_hosts.grid(column=2, row=2, padx=2, pady=2)


win_umanager = WinUManager()
win_umanager.open()
