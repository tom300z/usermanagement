from program.meth import *

# Reads the Database credentials from the configuration.
dbhost = conf["Database"]["host"]
dbuser = conf["Database"]["user"]
dbpassword = conf["Database"]["password"]
dbdatabase = "Umanager"


dbcon = MySQLdb.connect(host=dbhost, user=dbuser, passwd=dbpassword)
dbcon.autocommit(True)
dbcur = dbcon.cursor()
dbcur.execute("""SHOW DATABASES""")

createdb = None
try:
    dbcur.execute("""USE {}""".format(dbdatabase))
except:
    if platform.system() == "Linux":
        createdb = input("Database {} not found should it be created? [Y/N]: ".format(dbdatabase))
        if createdb in ["Y", "y", "Yes", "yes"]:
            log.info("Creating database \"{}\"".format(dbdatabase))
            os.system("mysql -u {} {}{} --host={} < {}".format(dbuser, "-p" if dbpassword not in ['', None, ' '] else "", dbpassword, dbhost, fullpath("database.sql")))
        else:
            log.critical("Database {} not found.".format(dbdatabase))
    else:
        log.critical("Database {} not found.".format(dbdatabase))
    try:
        dbcur.execute("""USE {}""".format(dbdatabase))
    except:
        log.critical("Database {} not found.".format(dbdatabase))


class List(object):
    def users(self):
        """Returns a list of users in the database, if the argument "all" is True the list will also include deleted
        users,"""
        dbcur.execute("""SELECT login FROM Umanager.user WHERE state!="{}" """.format("DELETED"))
        userlist = []
        data = dbcur.fetchall()
        for i in data:
            userlist.append(i[0])
        return userlist

    def groups(self):
        """Returns a list of groups in the database, if the argument "all" is True the list will also include deleted
        groups,"""
        dbcur.execute("""SELECT name FROM Umanager.group WHERE state!="{}" """.format("DELETED"))
        grouplist = []
        data = dbcur.fetchall()
        for i in data:
            grouplist.append(i[0])
        return grouplist

    def hosts(self):
        """Returns a list of users in the database, if the argument "all" is True the list will also include deleted
        users,"""
        dbcur.execute("""SELECT hostid FROM Umanager.host WHERE state!="{}" """.format("DELETED"))
        hostlist = []
        data = dbcur.fetchall()
        for i in data:
            hostlist.append(i[0])
        return hostlist


class User(object):
    def __init__(self, login=None):
        """Initializes the class, and sets all of its attributes"""
        self.login = login
        if self.indb():
            dbcur.execute("""SELECT * FROM user WHERE login="{}";""".format(self.login))
            data = dbcur.fetchall()[0]
            self.public_key = data[1]
            self.private_key = data[2]
            self._password = data[4]
            if data[5].upper() in ["ENABLED", "DISABLED", "DELETED"]:
                self.state = data[5].upper()
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[4]))
                self.state = "DISABLED"
        else:
            log.debug("User {} not found".format(self.login))
            self.public_key = ""
            self.private_key = ""
            self._password = ""
            self.state = "ENABLED"

    def add(self):
        """Adds the user to the database if it does not exist already"""
        if self.login not in [None, False, "", "None", "False"]:
            if re.fullmatch(r'[A-z0-9\-]*', self.login):
                if not self.indb():
                    self.state = "ENABLED"
                    self.clear()
                    dbcur.execute(
                        """INSERT INTO `Umanager`.`user` (public_key, private_key, login, `password`, state) VALUES ("{pubkey}", "{privkey}", "{login}", "{pw}", "{state}")""".format(
                            pubkey=self.public_key, privkey=self.private_key, login=self.login, pw=self._password, state=self.state))
                    dbcon.commit()
                    log.info("Added user \"{ul}\".".format(ul=self.login))
                else:
                    log.warning("User \"{}\" already exists, use update to update it.".format(self.login))
            else:
                log.warning("The username \"{}\" contains invalid characters.".format(self.login))
        else:
            log.warning("\"{}\" is not a valid username.".format(self.login))

    def update(self):
        """Updates the user in the database if it exists"""
        if self.indb():
            if self.state.upper() in ["ENABLED", "DISABLED", "DELETED"]:
                self.state = self.state.upper()
            else:
                log.warning("State \"{}\" not found setting it to DISABLED")
                self.state = "DISABLED"

            dbcur.execute(
                """UPDATE `Umanager`.`user` SET public_key="{pubkey}", private_key="{privkey}", password="{pw}", state="{state}" WHERE login="{login}\"""".format(
                    pubkey=self.public_key, privkey=self.private_key, login=self.login, pw=self._password, state=self.state))
            dbcon.commit()
            log.info("Updated user \"{}\".".format(self.login))
        else:
            log.warning("User \"{}\" does not exist, please add it first".format(self.login))

    def remove(self):
        """Sets the user state to deleted if it exists."""
        if self.indb():
            self.state = "DELETED"
            dbcur.execute(
                """UPDATE `Umanager`.`user` SET state="{state}" WHERE login="{login}" AND state!="DELETED" """.format(
                    login=self.login, state=self.state))
            dbcur.execute(
                """DELETE FROM `Umanager`.`usergroups` WHERE userLogin="{ul}" """.format(ul=self.login))
            dbcon.commit()
            log.info("Removed user \"{}\".".format(self.login))
        else:
            log.warning("User \"{}\" does not exist, no need to remove it".format(self.login))

    def clear(self):
        """Clears deleted users with the same login from the database"""
        dbcur.execute("""DELETE FROM `Umanager`.`user` WHERE login=\"{}\" AND state=\"DELETED\"""".format(self.login))

    def indb(self):
        """Checks whether the user exists in the database and returns the according boolean value."""
        if self.login in List().users():
            return True
        else:
            return False

    def grouplist(self):
        """Returns a list of all groups this user belongs to."""
        grouplist = []
        dbcur.execute("""SELECT * FROM `Umanager`.`usergroups` WHERE userLogin="{}" """.format(self.login))
        data = dbcur.fetchall()
        for row in data:
            groupName = row[2]
            grouplist.append(groupName)
        return grouplist

    def infodict(self):
        """Rezrns a dictionary with user information."""
        datadict = {}
        if self.indb():
            datadict["public_key"] = self.public_key
            datadict["private_key"] = self.private_key
            datadict["login"] = self.login
            datadict["password"] = self._password
            datadict["state"] = self.state
            datadict["groups"] = self.grouplist()
        else:
            log.info("User \"{}\" does not exist".format(self.login))
        return datadict


class Group(object):
    def __init__(self, name=None):
        """Initializes the class, and sets all of its attributes."""
        self.name = name
        if self.indb():
            dbcur.execute("""SELECT * FROM `Umanager`.`group` WHERE name="{}";""".format(self.name))
            data = dbcur.fetchall()[0]
            if data[1].upper() in ["ENABLED", "DISABLED", "DELETED"]:
                self.state = data[1]
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[4]))
                self.state = "DISABLED"
        else:
            log.debug("Group {} not found".format(self.name))
            self.state = "ENABLED"

    def add(self):
        """Adds the group to the database."""
        if self.name not in [None, False, "", "None", "False"]:
            if re.fullmatch(r'[A-z0-9\-]*', self.name):
                if not self.indb():
                    self.state = "ENABLED"
                    self.clear()
                    dbcur.execute("""INSERT INTO `Umanager`.`group` (name, state) VALUES ("{gn}", "{state}")""".format(
                        gn=self.name, state=self.state))
                    dbcon.commit()
                    log.info("Added group \"{}\".".format(self.name))
                else:
                    log.warning("Group \"{}\" already exists.".format(self.name))
            else:
                log.warning("The groupname \"{}\" contains invalid characters.".format(self.name))
        else:
            log.warning("\"{}\" is not a valid groupname.".format(self.name))

    def remove(self):
        """Sets the state of the group to deleted."""
        if self.indb():
            self.state = "DELETED"
            dbcur.execute(
                """UPDATE `Umanager`.`group` SET state="{state}" WHERE name="{name}" AND state!="DELETED" """.format(
                    name=self.name, state=self.state))
            dbcur.execute(
                """DELETE FROM `Umanager`.`usergroups` WHERE groupName="{gn}" """.format(gn=self.name))
            dbcon.commit()
            log.info("Removed group \"{}\".".format(self.name))
        else:
            log.warning("Group \"{}\" does not exist, no need to remove it".format(self.name))

    def clear(self):
        """Removes all deleted groups with the same name from the database."""
        dbcur.execute("""DELETE FROM `Umanager`.`group` WHERE name=\"{}\" AND state=\"DELETED\"""".format(self.name))

    def adduser(self, user):
        """Adds the given user to this group."""
        if user.indb():
            if self.indb():
                if user.login not in self.memberlist():
                    dbcur.execute(
                        """INSERT INTO `Umanager`.`usergroups` (userLogin, groupName) VALUES ("{un}", "{gn}")""".format(
                            un=user.login, gn=self.name))
                    log.info("User \"{un}\" is now part of the group \"{gn}\"".format(un=user.login, gn=self.name))
                else:
                    log.info("User \"{un}\" is already part of the group \"{gn}\"".format(un=user.login, gn=self.name))
            else:
                log.warning("Group \"{gn}\" does not exist".format(gn=self.name))
        else:
            log.warning("User \"{un}\" does not exist".format(un=user.login))

    def removeuser(self, user):
        """Removes the given user from this group."""
        if user.indb():
            if self.indb():
                if user.login in self.memberlist():
                    dbcur.execute(
                        """DELETE FROM `Umanager`.`usergroups` WHERE groupName="{gn}" AND userLogin="{un}" """.format(
                            un=user.login, gn=self.name))
                    log.info(
                        "User \"{un}\" is no longer part of the group \"{gn}\"".format(un=user.login, gn=self.name))
                else:
                    log.info("User \"{un}\" is not part of the group \"{gn}\"".format(un=user.login, gn=self.name))
            else:
                log.warning("Group \"{gn}\" does not exist".format(gn=self.name))
        else:
            log.warning("User \"{un}\" does not exist".format(un=user.login))

    def indb(self):
        """Checks whether the group exists in the database and returns the according boolean value."""
        if self.name in List().groups():
            return True
        else:
            return False

    def memberlist(self):
        """Returns a list of all members of this group."""
        memberlist = []
        dbcur.execute("""SELECT * FROM `Umanager`.`usergroups` WHERE groupName="{}" """.format(self.name))
        data = dbcur.fetchall()
        for row in data:
            userName = row[1]
            memberlist.append(userName)
        return memberlist

    def infodict(self):
        """Returns a dictionary with information about the group."""
        datadict = {}
        if self.indb():
            datadict["name"] = self.name
            datadict["members"] = self.memberlist()
        else:
            log.info("Group \"{}\" does not exist".format(self.name))
        return datadict


class Host(object):
    def __init__(self, hostid=None):
        """Initializes the class, and sets all of its attributes"""
        self.hostid = hostid
        if self.indb():
            dbcur.execute("""SELECT * FROM host WHERE hostid="{}";""".format(self.hostid))
            data = dbcur.fetchall()[0]
            if data[2].upper() in ["ENABLED", "DISABLED", "DELETED"]:
                self.state = data[2]
            else:
                log.warning("Invalid state \"{}\" setting it to DISABLED".format(data[2]))
                self.state = "DISABLED"
        else:
            log.debug("Host {} not found".format(self.hostid))
            self.state = "ENABLED"

    def add(self):
        """Adds the host to the database."""
        if self.hostid not in [None, False, "", "None", "False"]:
            if re.fullmatch(r'[A-z0-9\-_.]*', self.hostid):
                if not self.indb():
                    self.state = "ENABLED"
                    self.clear()
                    dbcur.execute(
                        """INSERT INTO `Umanager`.`host` (hostid, state) VALUES ("{}","{}")""".format(
                            self.hostid, self.state))
                    dbcur.execute("""CREATE USER IF NOT EXISTS 'client'@"{}" IDENTIFIED BY "" """.format(self.hostid))
                    dbcur.execute(
                        """GRANT SELECT,SHOW VIEW ON *.* TO 'client'@"{}" IDENTIFIED BY "" """.format(self.hostid))
                    dbcon.commit()
                    log.info("Added host \"{}\".".format(self.hostid))
                else:
                    log.warning("Host \"{}\" already exists, use update to update it.".format(self.hostid))
            else:
                log.warning(" \"{}\" contains invalid characters.".format(self.hostid))
        else:
            log.warning("\"{}\" is not valid.".format(self.hostid))

    def remove(self):
        """Sets the state of this host to deleted."""
        if self.indb():
            self.state = "DELETED"
            dbcur.execute(
                """UPDATE `Umanager`.`host` SET state="{state}" WHERE hostid="{hostid}" AND state!="DELETED" """.format(
                    hostid=self.hostid, state=self.state))
            dbcur.execute(
                """DROP USER IF EXISTS 'client'@"{}" """.format(self.hostid))
            dbcon.commit()
            log.info("Removed Host \"{}\".".format(self.hostid))
        else:
            log.warning("Host \"{}\" does not exist, no need to remove it".format(self.hostid))

    def clear(self):
        """Removes all deleted hosts with the same hostid from the database."""
        dbcur.execute("""DELETE FROM `Umanager`.`host` WHERE hostid=\"{}\" AND state=\"DELETED\"""".format(self.hostid))

    def indb(self):
        """Checks whether the host exists in the database and returns the according boolean value."""
        if self.hostid in List().hosts():
            return True
        else:
            return False

    def infodict(self):
        """Returns a dictionary with information about this host."""
        datadict = {}
        if self.indb():
            datadict["hostid"] = self.hostid
            datadict["state"] = self.state
        else:
            log.info("Host \"{}\" does not exist".format(self.hostid))
        return datadict
