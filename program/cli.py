import shlex
from cmd import Cmd
from program.dbmanager import *


class argument(object):
    """Represents a command line argument. which always has a name and a value, it also has a boolean representation
    that shows whether the argument was specified or not."""
    def __init__(self, name, value, bool=False):
        """Initializes the class, and sets all of its attributes"""
        self.name = name
        self.value = value
        if bool in [True, False]:
            self.bool = bool
        elif value in [True, False]:
            self.bool = value
        else:
            self.bool = True

    def __repr__(self):
        """Returns the value of the argument."""
        return str(self.value)

    def __str__(self):
        """Returns the value of the argument."""
        return str(self.value)

    def __bool__(self):
        """Returns the boolean value of the argument."""
        return self.bool


class parseargs(object):
    """Takes a raw string of arguments and parses them into argument objects."""
    def __init__(self, args):
        """Initializes the class, and sets all of its attributes"""
        rawarglist = shlex.split(args)
        self.arglist = []
        self.newnonearg = argument("", "", bool=False)
        for i, arg in enumerate(rawarglist):
            bool = True
            if "=" in arg:
                name = arg.split("=")[0]
                value = arg.split("=")[1]
            else:
                name = arg
                value = ""
            arg = argument(name, value, bool=bool)
            self.arglist.append(arg)
            setattr(self, name, arg)

    def __getitem__(self, item):
        """Called when the object is called with an index (obj[0]), returns the according argument."""
        if item <= len(self.arglist)-1:
            return self.arglist[item]
        else:
            return self.newnonearg

    def __repr__(self):
        """Returns the arguments as a list."""
        return self.arglist

    def __str__(self):
        """Returns the arguments as a stringified list."""
        return str(self.arglist)

    def __iter__(self):
        """Called when the object is iterated over, returns the according arguments."""
        return self.arglist.__iter__()

    def __getattr__(self, attr):
        """Called when an attribute of the object is called, used to dynamically assign all arguments as attributes."""
        returnnone = False
        for arg in self.arglist:
            if arg.name == attr:
                return arg
        return argument("", "", bool=False)


class UManShell(Cmd):
    """Creates a simple shell that is used to manage users."""
    def do_show(self, args):
        """Usage: show user(s)|group(s)|host(s) <username>[,<username>,..]|<groupname>[,<groupname>,..]|<domain_or_ip>[,<domain_or_ip>,..]
        Shows all attributes of one ore more users, groups or hosts."""
        args = parseargs(args)
        type = args[0].name
        if type in ["user", "users"]:
            users = args[1].name
            userlist = users.split(",")
            for username in userlist:
                data = User(login=username).infodict()
                for key in data:
                    print("{:<15}{:<20}".format(key + ":", str(data[key])))
        elif type in ["group", "groups"]:
            groups = args[1].name
            grouplist = groups.split(",")
            for groupname in grouplist:
                data = Group(name=groupname).infodict()
                for key in data:
                    print("{:<15}{:<20}".format(key + ":", str(data[key])))
        elif type in ["host", "hosts"]:
            hosts = args[1].name
            hostlist = hosts.split(",")
            for hostid in hostlist:
                data = Host(hostid=hostid).infodict()
                for key in data:
                    print("{:<15}{:<20}".format(key + ":", str(data[key])))
        else:
            log.info("Usage: help show")

    def do_list(self, args):
        """Usage: list user(s)|group(s)|host(s)
       Lists all users, groups or hosts."""
        args = parseargs(args)
        type = args[0].name
        if type in ["user", "users"]:
            print("LIST OF ALL USERS ATTACHED:\n" + "\n".join(List().users()))
        elif type in ["group", "groups"]:
            print("LIST OF ALL GROUPS ATTACHED:\n" + "\n".join(List().groups()))
        elif type in ["host", "hosts"]:
            print("LIST OF ALL HOSTS ATTACHED:\n" + "\n".join(List().hosts()))
        else:
            log.info("Usage: help list")

    def do_add(self, args):
        """Usage: add user(s) <username>[,<username>,..]
[ public_key=    Specify user public key in quotes ("ssh-rsa ...")
  private_key=     Specify user private key
  password=     Specify user password ]
        Adds one ore more new users.

Usage: add group(s) <groupname>[,<groupname>,..]
        Adds one ore more new groups.

Usage: add host(s) <domain_or_ip>[,<domain_or_ip>,..]
        Adds one ore more new hosts.

Usage: add <username>[,<username>,..] to <groupname>[,<groupname>,..]
        Assigns user(s) to group(s)."""
        args = parseargs(args)
        type = args[0].name
        if type in ["user", "users"]:
            users = args[1].name
            userlist = users.split(",")
            for username in userlist:
                user = User()
                user.login = username
                user.public_key = str(args.public_key)
                user.private_key = str(args.private_key)
                if args.password:
                    password = str(args.password)
                elif conf.has_option("User", "default_password"):
                    if conf["User"]["default_password"] == "RANDOM":
                        password = randomstring(length=10)
                    else:
                        password = str(conf["User"]["default_password"])
                else:
                    password = ""
                user._password = password
                user.add()
                group = Group(name=user.login)
                group.add()
                group.adduser(user)
                print("User password:", password)
        elif type in ["group", "groups"]:
            groups = args[1].name
            grouplist = groups.split(",")
            for groupname in grouplist:
                group = Group()
                group.name = str(groupname)
                group.add()
        elif type in ["host", "hosts"]:
            hosts = args[1].name
            hostlist = hosts.split(",")
            for hostid in hostlist:
                host = Host()
                host.hostid = str(hostid)
                host.add()
        elif args[1].name == "to":
            users = args[0].name
            userlist = users.split(",")
            groups = args[2].name
            grouplist = groups.split(",")
            for group in grouplist:
                groupobj = Group(name=group)
                for user in userlist:
                    if group not in List().users():
                        groupobj.adduser(User(login=user))
                    else:
                        log.warning("Cannot add user \"{}\" to the primary group \"{}\".".format(user, group))
        else:
            log.info("Usage: help add")

    def do_update(self, args):
        """Usage: update user(s) <username>[,<username>,..]
[ public_key=    Specify new user public key in quotes ("ssh-rsa ...")
  private_key=     Specify new user private key
  password=     Specify new user password ]
        Changes attributes of one or more users."""
        args = parseargs(args)
        type = args[0].name
        if type in ["user", "users"]:
            users = args[1].name
            userlist = users.split(",")
            for username in userlist:
                user = User(login=str(username))
                if args.public_key:
                    user.public_key = str(args.public_key)
                if args.private_key:
                    user.private_key = str(args.private_key)
                if args.password:
                    user._password = str(args.password)
                if args.state:
                    user.state = str(args.state)
                user.update()
        elif type in ["host", "hosts"]:
            hosts = args[1].name
            hostlist = hosts.split(",")
            for hostid in hostlist:
                host = Host(hostid=hostid)
                if args.hostname:
                    host.hostname = str(args.hostname)
                if args.protocol:
                    host.protocol = str(args.protocol)
                if args.port:
                    host.port = str(args.port)
                if args.state:
                    host.state = str(args.state)
                host.update()
        else:
            log.info("Usage: help update")

    def do_disable(self, args):
            """Usage: disable user(s) <username>[,<username>,..]
            Disables one or more users."""
            args = parseargs(args)
            type = args[0].name
            if type in ["user", "users"]:
                users = args[1].name
                userlist = users.split(",")
                for username in userlist:
                    user = User(login=str(username))
                    user.state = "DISABLED"
                    user.update()
            else:
                log.info("Usage: help disable")

    def do_enable(self, args):
            """Usage: enable user(s) <username>[,<username>,..]
            Enables one or more users."""
            args = parseargs(args)
            type = args[0].name
            if type in ["user", "users"]:
                users = args[1].name
                userlist = users.split(",")
                for username in userlist:
                    user = User(login=str(username))
                    user.state = "ENABLED"
                    user.update()
            else:
                log.info("Usage: help disable")

    def do_remove(self, args):
        """Usage: remove user(s) <username>[,<username>,..]
        Removes one or more users.

Usage: remove group(s) <groupname>[,<groupname>,..]
        Removes one or more groups.

Usage: remove host(s) <domain_or_ip>[,<domain_or_ip>,..]
        Removes one or more hosts.

Usage: remove <username>[,<username>,..] from <groupname>[,<groupname>,..]
        Removes user(s) from group(s)."""
        args = parseargs(args)
        type = args[0].name
        if type in ["user", "users"]:
            users = args[1].name
            userlist = users.split(",")
            for username in userlist:
                user = User(login=username)
                group = Group(name=user.login)
                group.removeuser(user)
                group.remove()
                user.remove()
        elif type in ["group", "groups"]:
            groups = args[1].name
            grouplist = groups.split(",")
            for groupname in grouplist:
                group = Group(name=groupname)
                if group.name not in List().users():
                    group.remove()
                else:
                    log.warning("Can't remove the primary group of user \"{}\"".format(group.name))
        elif type in ["host", "hosts"]:
            hosts = args[1].name
            hostlist = hosts.split(",")
            for hostid in hostlist:
                host = Host(hostid=hostid)
                host.remove()
        elif args[1].name == "from":
            users = args[0].name
            userlist = users.split(",")
            groups = args[2].name
            grouplist = groups.split(",")
            for group in grouplist:
                groupobj = Group(name=group)
                for user in userlist:
                    if group not in List().users():
                        groupobj.removeuser(User(login=user))
                    else:
                        log.warning("Cannot remove user \"{}\" from the primary group \"{}\".".format(user, group))
        else:
            log.info("Usage: help remove")

    def do_quit(self, args):
        """Quitting the UManager shell"""
        log.info("Quitting the UManager shell")
        exitprogramm()

    def do_exit(self, args):
        self.do_quit(None)

    def do_EOF(self, args):
        return True


# Creates the shell object and starts its loop, if the testmode is enabled the prompt will be empty.
prompt = UManShell()
if conf["Misc"]["testmode"] == "True":
    prompt.prompt = ""
else:
    prompt.prompt = "UManager CLI> "
prompt.cmdloop()
