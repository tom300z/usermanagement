import argparse
from program.meth import *

# Setting the interface based on the configuration.
if conf.has_option("Misc", "default_interface"):
    if conf["Misc"]["default_interface"] not in ["cli", "gui"]:
        log.warning("No such interface \"{}\", setting it to \"cli\"".format(conf["Misc"]["default_interface"]))
else:
    log.debug("Default interface not set, setting it to \"cli\"")
    conf["Misc"]["default_interface"] = "cli"
default_interface = conf["Misc"]["default_interface"]

# Initializes The argument parser.
parser = argparse.ArgumentParser()
parser.add_argument("--cli", action="store_true", help="Use the command line interface")
parser.add_argument("--gui", action="store_true", help="Use the graphical user interface")
parser.add_argument("-v", "--verbose", action="store_true", help="Display debug information")
parser.add_argument("--testmode", action="store_true", help="Run in testing mode")
args = parser.parse_args()

# Sets the interface based on command line parameters.
if args.cli:
    interface = "cli"
elif args.gui:
    interface = "gui"
else:
    interface = default_interface

# Sets the logging level to debug if the "verbose" argument is given.
if args.verbose:
    log.setLevel(logging.DEBUG)
    log.debug("Starting in verbose mode")

# Enables testmode to debug if the "testmode" argument is given.
if args.testmode:
    conf["Misc"]["testmode"] = "True"
else:
    conf["Misc"]["testmode"] = "False"

# Starts the chosen interface.
if interface == "cli":
    log.debug("Using the command line interface")
elif interface == "gui":
    log.debug("Using the graphical user interface")
exec("from program.{} import *".format(interface))
