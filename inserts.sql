use umanager;
insert into umanager.user (public_key, private_key, login, `password`, state)
values ("public", "private", "tom300z", "pass", "ENABLED");
insert into umanager.user (public_key, private_key, login, `password`, `state`)
values ("public", "private", "admin", "supersecret", "ENABLED");
insert into umanager.user (public_key, private_key, login, `password`, `state`)
values ("public", "private", "anonymous", "WeAreLegion", "DELETED");
insert into umanager.host (hostid, state)
values ("srv1.test.localhost", "ENABLED");
insert into umanager.group (name, state)
values ("admingroup", "ENABLED");
insert into umanager.group (name, state)
values ("Developer", "ENABLED");
insert into umanager.usergroups (userLogin, groupName)
values ("tom300z", "Developer");
insert into umanager.usergroups (userLogin, groupName)
values ("admin", "admingroup");
insert into umanager.usergroups (userLogin, groupName)
values ("anonymous", "admingroup");
insert into umanager.usergroups (userLogin, groupName)
values ("tom300z", "tom300z");
insert into umanager.usergroups (userLogin, groupName)
values ("admin", "admin");
insert into umanager.usergroups (userLogin, groupName)
values ("anonymous", "anonymous");